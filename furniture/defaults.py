
from collections import OrderedDict

from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

FURNITURE_SHOP_NAME = 'furniture'
FURNITURE_SHOP_TAGLINE = ''
FURNITURE_HOMEPAGE = reverse_lazy('promotions:home')

FURNITURE_USE_LESS = False

DEFAULT_CURRENCY = 'Rs'
FURNITURE_DEFAULT_CURRENCY = 'Rs'

AUTH_USER_MODEL = 'customer.User'

# Slug
FURNITURE_SLUG_FUNCTION = 'core.utils.default_slugifier'
FURNITURE_SLUG_ALLOW_UNICODE = False
FURNITURE_SLUG_MAP = {}
FURNITURE_SLUG_BLACKLIST = []
FURNITURE_IMAGE_FOLDER = 'images/products/%Y/%m/'
FURNITURE_MISSING_IMAGE_URL = 'image_not_found.jpg'
FURNITURE_UPLOAD_ROOT = '/tmp'

# Address settings
FURNITURE_REQUIRED_ADDRESS_FIELDS = ('first_name', 'last_name', 'line1',
                                 'line4', 'postcode', 'country')


# Basket settings
FURNITURE_BASKET_COOKIE_LIFETIME = 7 * 24 * 60 * 60
FURNITURE_BASKET_COOKIE_OPEN = 'furniture_open_basket'
FURNITURE_BASKET_COOKIE_SECURE = False
FURNITURE_MAX_BASKET_QUANTITY_THRESHOLD = 10000

# Recently-viewed products
FURNITURE_RECENTLY_VIEWED_COOKIE_LIFETIME = 7 * 24 * 60 * 60
FURNITURE_RECENTLY_VIEWED_COOKIE_NAME = 'furniture_history'
FURNITURE_RECENTLY_VIEWED_COOKIE_SECURE = False
FURNITURE_RECENTLY_VIEWED_PRODUCTS = 20


# Pagination settings

FURNITURE_OFFERS_PER_PAGE = 20
FURNITURE_PRODUCTS_PER_PAGE = 20
FURNITURE_REVIEWS_PER_PAGE = 20
FURNITURE_NOTIFICATIONS_PER_PAGE = 20
FURNITURE_EMAILS_PER_PAGE = 20
FURNITURE_ORDERS_PER_PAGE = 20
FURNITURE_ADDRESSES_PER_PAGE = 20
FURNITURE_STOCK_ALERTS_PER_PAGE = 20
FURNITURE_DASHBOARD_ITEMS_PER_PAGE = 20

# Checkout
FURNITURE_ALLOW_ANON_CHECKOUT = False

# Promotions
FURNITURE_PROMOTION_POSITIONS = (('page', 'Page'),
                             ('right', 'Right-hand sidebar'),
                             ('left', 'Left-hand sidebar'))

# Reviews
FURNITURE_ALLOW_ANON_REVIEWS = True
FURNITURE_MODERATE_REVIEWS = False

# Accounts
FURNITURE_ACCOUNTS_REDIRECT_URL = 'customer:profile-view'
FURNITURE_COOKIES_DELETE_ON_LOGOUT = ['furniture_recently_viewed_products', ]
ACCOUNT_REDIRECT_URL = 'customer:profile-view'
LOGIN_URL = 'customer:login'
# This enables sending alert notifications/emails instantly when products get
# back in stock by listening to stock record update signals.
# This might impact performance for large numbers of stock record updates.
# Alternatively, the management command ``furniture_send_alerts`` can be used to
# run periodically, e.g. as a cron job. In this case eager alerts should be
# disabled.
FURNITURE_EAGER_ALERTS = True

#whislists
FURNITURE_HIDDEN_FEATURES = []

# Registration
FURNITURE_SEND_REGISTRATION_EMAIL = True
FURNITURE_FROM_EMAIL = 'furniture@example.com'

FURNITURE_PRODUCTS_PER_PAGE = 10
FURNITURE_PRODUCT_SEARCH_HANDLER = True

# Search facets
FURNITURE_SEARCH_FACETS = {
    'fields': OrderedDict([
        # The key for these dicts will be used when passing facet data
        # to the template. Same for the 'queries' dict below.
        ('product_class', {'name': _('Type'), 'field': 'product_class'}),
        ('rating', {'name': _('Rating'), 'field': 'rating'}),
        # You can specify an 'options' element that will be passed to the
        # SearchQuerySet.facet() call.
        # For instance, with Elasticsearch backend, 'options': {'order': 'term'}
        # will sort items in a facet by title instead of number of items.
        # It's hard to get 'missing' to work
        # correctly though as of Solr's hilarious syntax for selecting
        # items without a specific facet:
        # http://wiki.apache.org/solr/SimpleFacetParameters#facet.method
        # 'options': {'missing': 'true'}
    ]),
    'queries': OrderedDict([
        ('price_range',
         {
             'name': _('Price range'),
             'field': 'price',
             'queries': [
                 # This is a list of (name, query) tuples where the name will
                 # be displayed on the front-end.
                 (_('0 to 20000'), u'[0 TO 20000]'),
                 (_('20000 to 40000'), u'[20000 TO 40000]'),
                 (_('40000 to 60000'), u'[40000 TO 60000]'),
                 (_('60000+'), u'[60000 TO *]'),
             ]
         }),
    ]),
}


FURNITURE_PROMOTIONS_ENABLED = True
FURNITURE_PRODUCT_SEARCH_HANDLER = None
FURNITURE_DELETE_IMAGE_FILES = True

FURNITURE_PROMOTION_FOLDER = 'images/promotions/'

FURNITURE_SETTINGS = dict(
    [(k, v) for k, v in locals().items() if k.startswith('FURNITURE_')])

FURNITURE_DASHBOARD_NAVIGATION = [
    {
        'label': _('Dashboard'),
        'icon': 'icon-th-list',
        'url_name': 'dashboard:index',
    },
    {
        'label': _('Catalogue'),
        'icon': 'icon-sitemap',
        'children': [
            {
                'label': _('Products'),
                'url_name': 'dashboard:catalogue-product-list',
            },
            {
                'label': _('Product Types'),
                'url_name': 'dashboard:catalogue-class-list',
            },
            {
                'label': _('Categories'),
                'url_name': 'dashboard:catalogue-category-list',
            },
            {
                'label': _('Ranges'),
                'url_name': 'dashboard:range-list',
            },
            {
                'label': _('Low stock alerts'),
                'url_name': 'dashboard:stock-alert-list',
            },
        ]
    },
    {
        'label': _('Fulfilment'),
        'icon': 'icon-shopping-cart',
        'children': [
            {
                'label': _('Orders'),
                'url_name': 'dashboard:order-list',
            },
            {
                'label': _('Statistics'),
                'url_name': 'dashboard:order-stats',
            },
            {
                'label': _('Partners'),
                'url_name': 'dashboard:partner-list',
            },
            # The shipping method dashboard is disabled by default as it might
            # be confusing. Weight-based shipping methods aren't hooked into
            # the shipping repository by default (as it would make
            # customising the repository slightly more difficult).
            # {
            #     'label': _('Shipping charges'),
            #     'url_name': 'dashboard:shipping-method-list',
            # },
        ]
    },
    {
        'label': _('Customers'),
        'icon': 'icon-group',
        'children': [
            {
                'label': _('Customers'),
                'url_name': 'dashboard:users-index',
            },
            {
                'label': _('Stock alert requests'),
                'url_name': 'dashboard:user-alert-list',
            },
        ]
    },
    {
        'label': _('Offers'),
        'icon': 'icon-bullhorn',
        'children': [
            {
                'label': _('Offers'),
                'url_name': 'dashboard:offer-list',
            },
            {
                'label': _('Vouchers'),
                'url_name': 'dashboard:voucher-list',
            },
        ],
    },
    {
        'label': _('Content'),
        'icon': 'icon-folder-close',
        'children': [
            {
                'label': _('Content blocks'),
                'url_name': 'dashboard:promotion-list',
            },
            {
                'label': _('Content blocks by page'),
                'url_name': 'dashboard:promotion-list-by-page',
            },
            {
                'label': _('Pages'),
                'url_name': 'dashboard:page-list',
            },
            {
                'label': _('Email templates'),
                'url_name': 'dashboard:comms-list',
            },
            {
                'label': _('Reviews'),
                'url_name': 'dashboard:reviews-list',
            },
        ]
    },
    {
        'label': _('Reports'),
        'icon': 'icon-bar-chart',
        'url_name': 'dashboard:reports-index',
    },
]

FURNITURE_DASHBOARD_DEFAULT_ACCESS_FUNCTION = 'apps.dashboard.nav.default_access_fn'  # noqa
