"""furniture URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import reverse_lazy

from views.decorators import login_forbidden
from apps.customer.forms import PasswordResetForm, SetPasswordForm

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=settings.HOME_PAGE_URL, permanent=False)),
    url(r'^catalogue/', include('apps.catalogue.urls', namespace='catalogue')),
    url(r'^customer/', include('apps.customer.urls', namespace='customer')),
    url(r'^basket/', include('apps.basket.urls', namespace='basket')),
    url(r'^checkout/', include('apps.checkout.urls', namespace='checkout')),
    url(r'^dashboard/', include('apps.dashboard.urls', namespace='dashboard')),
    url(r'^offer/', include('apps.offer.urls', namespace='offer')),
    url(r'^promotion/', include('apps.promotions.urls', namespace='promotions')),
    url(r'^search/', include('apps.search.urls', namespace='search')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^password-reset/$',
        login_forbidden(auth_views.password_reset),
        {'password_reset_form': PasswordResetForm,
         'post_reset_redirect': reverse_lazy('password-reset-done')},
        name='password-reset'),
    url(r'^password-reset/done/$',
        login_forbidden(auth_views.password_reset_done),
        name='password-reset-done'),
    url(r'^password-reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        login_forbidden(auth_views.password_reset_confirm),
        {
            'post_reset_redirect': reverse_lazy('password-reset-complete'),
            'set_password_form': SetPasswordForm,
        },
        name='password-reset-confirm'),
    url(r'^password-reset/complete/$',
        login_forbidden(auth_views.password_reset_complete),
        name='password-reset-complete'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
        document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)
