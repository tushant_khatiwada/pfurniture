import os

# Use 'dev', 'beta', or 'final' as the 4th element to indicate release type.
VERSION = (1, 6, 0, 'dev')


def get_short_version():
    return '%s.%s' % (VERSION[0], VERSION[1])


def get_version():
    version = '%s.%s' % (VERSION[0], VERSION[1])
    # Append 3rd digit if > 0
    if VERSION[2]:
        version = '%s.%s' % (version, VERSION[2])
    elif VERSION[3] != 'final':
        version = '%s %s' % (version, VERSION[3])
        if len(VERSION) == 5:
            version = '%s %s' % (version, VERSION[4])
    return version


# Cheeky setting that allows each template to be accessible by two paths.
# Eg: the template 'furniture/templates/furniture/base.html' can be accessed via both
# 'base.html' and 'furniture/base.html'.  This allows furniture's templates to be
# extended by templates with the same filename
FURNITURE_MAIN_TEMPLATE_DIR = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), 'templates')

FURNITURE_CORE_APPS = [
    'apps',
    'apps.analytics',
    'apps.address',
    'apps.basket',
    'apps.catalogue',
    'apps.catalogue.reviews',
    'apps.checkout',
    'apps.customer',
    'apps.offer',
    'apps.order',
    'apps.partner',
    'apps.payment',
    'apps.promotions',
    'apps.search',
    'apps.shipping',
    'apps.voucher',
    'apps.wishlists',
    'apps.dashboard',
    'apps.dashboard.reports',
    'apps.dashboard.users',
    'apps.dashboard.orders',
    'apps.dashboard.promotions',
    'apps.dashboard.catalogue',
    'apps.dashboard.offers',
    'apps.dashboard.partners',
    'apps.dashboard.pages',
    'apps.dashboard.ranges',
    'apps.dashboard.reviews',
    'apps.dashboard.vouchers',
    'apps.dashboard.communications',
    'apps.dashboard.shipping',
    # 3rd-party apps that furniture depends on
    'django_tables2',
    'sorl.thumbnail',
    'haystack',
    'treebeard',
    'versatileimagefield',
    'widget_tweaks',
]


def get_core_apps(overrides=None):
    """
    Return a list of furniture's apps amended with any passed overrides
    """
    if not overrides:
        return FURNITURE_CORE_APPS

    # Conservative import to ensure that this file can be loaded
    # without the presence Django.
    from django.utils import six
    if isinstance(overrides, six.string_types):
        raise ValueError(
            "get_core_apps expects a list or tuple of apps "
            "to override")

    def get_app_label(app_label, overrides):
        print('app_label', app_label, overrides);
        pattern = app_label.replace('furniture.apps.', '')
        for override in overrides:
            if override.endswith(pattern):
                if 'dashboard' in override and 'dashboard' not in pattern:
                    continue
                return override
        return app_label

    apps = []
    for app_label in FURNITURE_CORE_APPS:
        apps.append(get_app_label(app_label, overrides))
    return apps
