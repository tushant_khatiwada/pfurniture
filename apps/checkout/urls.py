from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),

    # Shipping/user address views
    url(r'shipping-address/$',
        views.ShippingAddressView.as_view(), name='shipping-address'),
    url(r'user-address/edit/(?P<pk>\d+)/$',
        views.UserAddressUpdateView.as_view(),
        name='user-address-update'),
    url(r'user-address/delete/(?P<pk>\d+)/$',
        views.UserAddressDeleteView.as_view(),
        name='user-address-delete'),

    # Shipping method views
    url(r'shipping-method/$',
        views.ShippingMethodView.as_view(), name='shipping-method'),

    # Payment views
    url(r'payment-method/$',
        views.PaymentMethodView.as_view(), name='payment-method'),
    url(r'payment-details/$',
        views.PaymentDetailsView.as_view(), name='payment-details'),
    #
    # # Preview and thankyou
    url(r'preview/$',
        views.PaymentDetailsView.as_view(preview=True),
        name='preview'),
    url(r'thank-you/$', views.ThankYouView.as_view(),
        name='thank-you'),
]



def get_url_decorator(self, pattern):
    if not settings.FURNITURE_ALLOW_ANON_CHECKOUT:
        return login_required
    if pattern.name.startswith('user-address'):
        return login_required
    return None
