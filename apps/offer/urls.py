from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.OfferListView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', views.OfferDetailView.as_view(),
        name='detail'),
]
