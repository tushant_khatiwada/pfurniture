# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-02-01 16:08
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('address', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAddress',
            fields=[
                ('shippingaddress_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='order.ShippingAddress')),
                ('is_default_for_shipping', models.BooleanField(default=False, verbose_name='Default shipping address?')),
                ('is_default_for_billing', models.BooleanField(default=False, verbose_name='Default billing address?')),
                ('num_orders_as_shipping_address', models.PositiveIntegerField(default=0, verbose_name='Number of Orders as Shipping Address')),
                ('num_orders_as_billing_address', models.PositiveIntegerField(default=0, verbose_name='Number of Orders as Billing Address')),
                ('hash', models.CharField(db_index=True, editable=False, max_length=255, verbose_name='Address Hash')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date Created')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='addresses', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'User address',
                'verbose_name_plural': 'User addresses',
                'ordering': ['-num_orders_as_shipping_address'],
            },
            bases=('order.shippingaddress',),
        ),
        migrations.AddField(
            model_name='address',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='address.Country', verbose_name='Country'),
        ),
        migrations.AlterUniqueTogether(
            name='useraddress',
            unique_together=set([('user', 'hash')]),
        ),
    ]
