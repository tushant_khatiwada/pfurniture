from django.contrib import admin

from core.loading import get_model

Partner = get_model('partner', 'Partner')
PartnerAddress = get_model('partner', 'PartnerAddress')
StockRecord = get_model('partner', 'StockRecord')


class StockRecordAdmin(admin.ModelAdmin):
    list_display = ('product', 'partner', 'partner_sku', 'price_excl_tax',
                    'cost_price', 'num_in_stock')
    list_filter = ('partner',)

class PartnerAddressInline(admin.TabularInline):

    model = PartnerAddress
    extra = 1

class PartnerAdmin(admin.ModelAdmin):
    inlines = [PartnerAddressInline, ]

    class Meta:

        model = Partner

admin.site.register(Partner, PartnerAdmin)
# admin.site.register(PartnerAddress)
admin.site.register(StockRecord, StockRecordAdmin)
