from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.views import generic

from .notifications import views as notification_views
from .alerts import views as alert_views
from .wishlists import views as wishlists_views
from . import views

urlpatterns = [
    url(r'^login/$', views.AccountAuthView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^register/$', views.AccountRegistrationView.as_view(), name='register'),
    url(r'^$', login_required(views.AccountSummaryView.as_view()),
        name='summary'),
    url(r'^change-password/$',
        login_required(views.ChangePasswordView.as_view()),
        name='change-password'),

    # Profile
    url(r'^profile/$',
        login_required(views.ProfileView.as_view()),
        name='profile-view'),
    url(r'^profile/edit/$',
        login_required(views.ProfileUpdateView.as_view()),
        name='profile-update'),
    url(r'^profile/delete/$',
        login_required(views.ProfileDeleteView.as_view()),
        name='profile-delete'),

    # Order history
    url(r'^orders/$',
        login_required(views.OrderHistoryView.as_view()),
        name='order-list'),
    url(r'^order-status/(?P<order_number>[\w-]*)/(?P<hash>\w+)/$',
        views.AnonymousOrderDetailView.as_view(), name='anon-order'),
    url(r'^orders/(?P<order_number>[\w-]*)/$',
        login_required(views.OrderDetailView.as_view()),
        name='order'),
    url(r'^orders/(?P<order_number>[\w-]*)/(?P<line_id>\d+)$',
        login_required(views.OrderLineView.as_view()),
        name='order-line'),

    # Address book
    url(r'^addresses/$',
        login_required(views.AddressListView.as_view()),
        name='address-list'),
    url(r'^addresses/add/$',
        login_required(views.AddressCreateView.as_view()),
        name='address-create'),
    url(r'^addresses/(?P<pk>\d+)/$',
        login_required(views.AddressUpdateView.as_view()),
        name='address-detail'),
    url(r'^addresses/(?P<pk>\d+)/delete/$',
        login_required(views.AddressDeleteView.as_view()),
        name='address-delete'),
    url(r'^addresses/(?P<pk>\d+)/'
        r'(?P<action>default_for_(billing|shipping))/$',
        login_required(views.AddressChangeStatusView.as_view()),
        name='address-change-status'),

    # Email history
    url(r'^emails/$',
        login_required(views.EmailHistoryView.as_view()),
        name='email-list'),
    url(r'^emails/(?P<email_id>\d+)/$',
        login_required(views.EmailDetailView.as_view()),
        name='email-detail'),

    # Notifications
    # Redirect to notification inbox
    url(r'^notifications/$', generic.RedirectView.as_view(
        url='/accounts/notifications/inbox/', permanent=False)),
    url(r'^notifications/inbox/$',
        login_required(notification_views.InboxView.as_view()),
        name='notifications-inbox'),
    url(r'^notifications/archive/$',
        login_required(notification_views.ArchiveView.as_view()),
        name='notifications-archive'),
    url(r'^notifications/update/$',
        login_required(notification_views.UpdateView.as_view()),
        name='notifications-update'),
    url(r'^notifications/(?P<pk>\d+)/$',
        login_required(notification_views.DetailView.as_view()),
        name='notifications-detail'),

    # Alerts
    # Alerts can be setup by anonymous users: some views do not
    # require login
    url(r'^alerts/$',
        login_required(alert_views.ProductAlertListView.as_view()),
        name='alerts-list'),
    url(r'^alerts/create/(?P<pk>\d+)/$',
        alert_views.ProductAlertCreateView.as_view(),
        name='alert-create'),
    url(r'^alerts/confirm/(?P<key>[a-z0-9]+)/$',
        alert_views.ProductAlertConfirmView.as_view(),
        name='alerts-confirm'),
    url(r'^alerts/cancel/key/(?P<key>[a-z0-9]+)/$',
        alert_views.ProductAlertCancelView.as_view(),
        name='alerts-cancel-by-key'),
    url(r'^alerts/cancel/(?P<pk>[a-z0-9]+)/$',
        login_required(alert_views.ProductAlertCancelView.as_view()),
        name='alerts-cancel-by-pk'),

    # Wishlists
    url(r'wishlists/$',
        login_required(wishlists_views.WishListListView.as_view()),
        name='wishlists-list'),
    url(r'wishlists/add/(?P<product_pk>\d+)/$',
        login_required(wishlists_views.WishListAddProduct.as_view()),
        name='wishlists-add-product'),
    url(r'wishlists/(?P<key>[a-z0-9]+)/add/(?P<product_pk>\d+)/',
        login_required(wishlists_views.WishListAddProduct.as_view()),
        name='wishlists-add-product'),
    url(r'wishlists/create/$',
        login_required(wishlists_views.WishListCreateView.as_view()),
        name='wishlists-create'),
    url(r'wishlists/create/with-product/(?P<product_pk>\d+)/$',
        login_required(wishlists_views.WishListCreateView.as_view()),
        name='wishlists-create-with-product'),
    # Wishlists can be publicly shared, no login required
    url(r'wishlists/(?P<key>[a-z0-9]+)/$',
        wishlists_views.WishListDetailView.as_view(), name='wishlists-detail'),
    url(r'wishlists/(?P<key>[a-z0-9]+)/update/$',
        login_required(wishlists_views.WishListUpdateView.as_view()),
        name='wishlists-update'),
    url(r'wishlists/(?P<key>[a-z0-9]+)/delete/$',
        login_required(wishlists_views.WishListDeleteView.as_view()),
        name='wishlists-delete'),
    url(r'wishlists/(?P<key>[a-z0-9]+)/lines/(?P<line_pk>\d+)/delete/',
        login_required(wishlists_views.WishListRemoveProduct.as_view()),
        name='wishlists-remove-product'),
    url(r'wishlists/(?P<key>[a-z0-9]+)/products/(?P<product_pk>\d+)/'
        r'delete/',
        login_required(wishlists_views.WishListRemoveProduct.as_view()),
        name='wishlists-remove-product'),
    url(r'wishlists/(?P<key>[a-z0-9]+)/lines/(?P<line_pk>\d+)/move-to/'
        r'(?P<to_key>[a-z0-9]+)/$',
        login_required(wishlists_views.WishListMoveProductToAnotherWishList
                       .as_view()),
        name='wishlists-move-product-to-another')
]
