from django.conf.urls import url, include

from . import views
from .reviews import urls as review_urls
from apps.offer.views import RangeDetailView

urlpatterns = [
    # url(r'^$', views.home, name="home"),
    url(r'^$', views.CatalogueView.as_view(), name="index"),
    url(r'^(?P<slug>[\w-]*)/(?P<pk>\d+)/$', views.ProductDetailView.as_view(), name="product-detail"),
    url(r'^category/(?P<category_slug>[\w-]+(/[\w-]+)*)_(?P<pk>\d+)/$', views.ProductCategoryView.as_view(), name="category"),
    url(r'^category/(?P<category_slug>[\w-]+(/[\w-]+)*)/$', views.ProductCategoryView.as_view()),
    url(r'^ranges/(?P<slug>[\w-]+)/$', RangeDetailView.as_view(), name="range"),
    url(r'^(?P<product_slug>[\w-]*)_(?P<product_pk>\d+)/reviews/', include('apps.catalogue.reviews.urls'))
]
