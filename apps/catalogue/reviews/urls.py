from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.ProductReviewDetail.as_view(),
        name='reviews-detail'),
    url(r'^add/$', views.CreateProductReview.as_view(),
        name='reviews-add'),
    url(r'^(?P<pk>\d+)/vote/$',
        login_required(views.AddVoteView.as_view()),
        name='reviews-vote'),
    url(r'^$', views.ProductReviewList.as_view(), name='reviews-list'),
]
