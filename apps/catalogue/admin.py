#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.contrib import admin

from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from .models import Category, ProductClass, \
    ProductCategory, Product, ProductVariant, ProductImage, \
    VariantImage, ProductAttribute, ProductAttributeValue, \
    AttributeOptionGroup, AttributeOption, Option


class CategoryAdmin(TreeAdmin):

    form = movenodeform_factory(Category)
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name', )}


# class ProductCategoryAdmin(admin.ModelAdmin):
#     class Meta:
#         model = ProductCategory
# admin.site.register(ProductCategory, ProductCategoryAdmin)

class CategoryInline(admin.TabularInline):

    model = ProductCategory
    extra = 1


class AttributeInline(admin.TabularInline):

    model = ProductAttributeValue


class ProductAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'available_on',
        'is_published',
        'is_featured',
        'is_bestseller',
        'is_free_shipping',
    )
    prepopulated_fields = {'slug': ('name', )}
    inlines=[AttributeInline, CategoryInline,]
    class Meta:

        model = Product


class ProductImageAdmin(admin.ModelAdmin):

    class Meta:

        model = ProductImage


class ProductAttributeAdmin(admin.ModelAdmin):

    list_display = ('name', 'code', 'product_class', 'type')
    prepopulated_fields = {'code': ('name', )}

    class Meta:

        model = ProductAttribute


class ProductAttributeInline(admin.TabularInline):

    model = ProductAttribute
    extra = 2


class ProductAttributeValueAdmin(admin.ModelAdmin):

    list_display = ('product', 'attribute', 'value')


class ProductClassAdmin(admin.ModelAdmin):

    list_display = ('name', 'slug', 'requires_shipping', 'track_stock')
    inlines = [ProductAttributeInline]


class OptionAdmin(admin.ModelAdmin):

    class Meta:

        model = Option


class AttributeOptionInline(admin.TabularInline):

    model = AttributeOption


class AttributeOptionGroupAdmin(admin.ModelAdmin):

    list_display = ('name', 'option_summary')
    inlines = [AttributeOptionInline]


admin.site.register(ProductClass, ProductClassAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductAttribute, ProductAttributeAdmin)
admin.site.register(ProductAttributeValue, ProductAttributeValueAdmin)
admin.site.register(AttributeOptionGroup, AttributeOptionGroupAdmin)
admin.site.register(Option, OptionAdmin)
admin.site.register(ProductImage, ProductImageAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ProductCategory)
