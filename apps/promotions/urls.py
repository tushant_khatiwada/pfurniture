from django.conf.urls import url

from apps.promotions.models import KeywordPromotion, PagePromotion
from . import views

urlpatterns = [
    url(r'page-redirect/(?P<page_promotion_id>\d+)/$',
        views.RecordClickView.as_view(model=PagePromotion),
        name='page-click'),
    url(r'keyword-redirect/(?P<keyword_promotion_id>\d+)/$',
        views.RecordClickView.as_view(model=KeywordPromotion),
        name='keyword-click'),
    url(r'^$', views.HomeView.as_view(), name='home'),
]
