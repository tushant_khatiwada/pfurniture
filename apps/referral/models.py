import uuid

from django.db import models
from django.utils import timezone

from core.compat import AUTH_USER_MODEL

# Create your models here.
class Referral(models.Model):
    referred_by = models.ForeignKey(AUTH_USER_MODEL,
                                    blank=True,
                                    null=True,
                                    on_delete=models.CASCADE,
                                    verbose_name='referrer')
    referred_to = models.ForeignKey(AUTH_USER_MODEL,
                                    blank=True,
                                    null=True,
                                    verbose_name='referral')
    refer_code = models.UUIDField(default=uuid.uuid4, unique=True)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = (('referred_by', 'referred_to'),)
        verbose_name = 'Referral'
        verbose_name_plural = 'Referrals'

    def __str__(self):
        return 'Referred by {0} to {1}'.format(self.referred_by, self.referred_to)

