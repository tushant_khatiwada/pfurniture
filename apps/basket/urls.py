from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^$', views.BasketView.as_view(), name='summary'),
    url(r'^add/(?P<pk>\d+)/$', views.BasketAddView.as_view(), name='add'),
    url(r'^vouchers/add/$', views.VoucherAddView.as_view(),
        name='vouchers-add'),
    url(r'^vouchers/(?P<pk>\d+)/remove/$',
        views.VoucherRemoveView.as_view(), name='vouchers-remove'),
    url(r'^saved/$', login_required(views.SavedView.as_view()),
        name='saved'),
]
