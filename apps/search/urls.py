from django.conf.urls import url
from haystack.views import search_view_factory

from apps.search import facets
from . import views
from .forms import SearchForm


def get_sqs():
    """
    Return the SQS required by a the Haystack search view
    """
    return facets.base_sqs()

urlpatterns = [
    url(r'^$', search_view_factory(
        view_class=views.FacetedSearchView,
        form_class=SearchForm,
        searchqueryset=get_sqs()),
        name='search'),
]
