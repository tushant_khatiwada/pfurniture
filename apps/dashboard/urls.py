from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import AuthenticationForm

from core.loading import get_class
# from . import views

index_view = get_class('dashboard.views', 'IndexView')
reports_app = get_class('dashboard.reports.app', 'application')
orders_app = get_class('dashboard.orders.app', 'application')
users_app = get_class('dashboard.users.app', 'application')
catalogue_app = get_class('dashboard.catalogue.app', 'application')
promotions_app = get_class('dashboard.promotions.app', 'application')
pages_app = get_class('dashboard.pages.app', 'application')
partners_app = get_class('dashboard.partners.app', 'application')
offers_app = get_class('dashboard.offers.app', 'application')
ranges_app = get_class('dashboard.ranges.app', 'application')
reviews_app = get_class('dashboard.reviews.app', 'application')
vouchers_app = get_class('dashboard.vouchers.app', 'application')
comms_app = get_class('dashboard.communications.app', 'application')
shipping_app = get_class('dashboard.shipping.app', 'application')

urlpatterns = [
    url(r'^$', index_view.as_view(), name='index'),
    url(r'^catalogue/', catalogue_app.urls),
    url(r'^reports/', reports_app.urls),
    url(r'^orders/', orders_app.urls),
    url(r'^users/', users_app.urls),
    url(r'^content-blocks/', promotions_app.urls),
    url(r'^pages/', pages_app.urls),
    url(r'^partners/', partners_app.urls),
    url(r'^offers/', offers_app.urls),
    url(r'^ranges/', ranges_app.urls),
    url(r'^reviews/', reviews_app.urls),
    url(r'^vouchers/', vouchers_app.urls),
    url(r'^comms/', comms_app.urls),
    url(r'^shipping/', shipping_app.urls),

    url(r'^login/$', auth_views.login, {
        'template_name': 'dashboard/login.html',
        'authentication_form': AuthenticationForm,
    }, name='login'),
    url(r'^logout/$', auth_views.logout, {
        'next_page': '/',
    }, name='logout'),
]
