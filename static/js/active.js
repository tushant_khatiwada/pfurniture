(function($) {
  'use strict';

  $(function() {
    $('[data-toggle="popover"]').popover({
      html: true,
      trigger: 'hover',
      content: function() {
        return '<img src="' + $(this).data('img') + '" />';
      }
    })
  })
  $('.slider-range-price').each(function() {
    var min = jQuery(this).data('min');
    var max = jQuery(this).data('max');
    var unit = jQuery(this).data('unit');
    var value_min = jQuery(this).data('value-min');
    var value_max = jQuery(this).data('value-max');
    var label_result = jQuery(this).data('label-result');
    var t = $(this);
    $(this).slider({
      range: true,
      min: min,
      max: max,
      values: [
        value_min, value_max
      ],
      slide: function(event, ui) {
        var result = label_result + " " + unit + ui.values[0] + ' - ' + unit + ui.values[1];
        console.log(t);
        t.closest('.slider-range').find('.range-price').html(result);
      }
    });
  })
  $("a[href='#']").on('click', function($) {
    $.preventDefault();
  });
  var $window = $(window);
  $window.on('scroll', function() {
    if ($window.scrollTop() > 200) {
      $('.header_area').addClass('sticky');
      $('.header_area .sticky .mainmenu_area').fadeIn('slow');
    } else {
      $('.header_area').removeClass('sticky');
      $('.header_area .sticky .mainmenu_area').fadeOut('slow');
    }
  });
})(jQuery);
